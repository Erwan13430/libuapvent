cmake_minimum_required(VERSION 3.10)
project(uapvent VERSION 0.1)

set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED True)
set(CMAKE_CXX_STANDARD)

find_package(PkgConfig REQUIRED)
pkg_check_modules(CURL REQUIRED libcurl)

file(GLOB UAPVENT_SRC
  "./src/*.h"
  "./src/*.cc"
)

add_library(uapvent ${UAPVENT_SRC})

target_link_libraries(uapvent PUBLIC ${CURL_LIBRARIES})
target_include_directories(uapvent PUBLIC ${CURL_INCLUDE_DIRS})


