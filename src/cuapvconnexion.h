#ifndef CUAPV_CONNEXION_H
#define CUAPV_CONNEXION_H

#include <array>
#include <iostream>
#include <string>

enum Chercher {
    EDT_FAVORIS = 0,
    EDT_SALLE_LIBRE,
};

class CUAPVConnexion {
    public:
        static CUAPVConnexion *recupInstance();
        static void libererInstance();

        std::string uapvEnvoiTrame(Chercher, std::array<std::string, 4>* params = nullptr);
        bool uapvConnexion(std::string, std::string);

    private:
        CUAPVConnexion();
        ~CUAPVConnexion();

        static CUAPVConnexion *m_singleton;
        const std::string m_Url_Edt = "https://edt-api.univ-avignon.fr/app.php/api/";
        const std::string m_Url_Cas = "https://cas.univ-avignon.fr/cas/login?service=https://edt.univ-avignon.fr/login";

        std::string m_billet;
        std::string m_jeton;
};


#endif
