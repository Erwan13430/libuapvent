#ifndef CCURL_H
#define CCURL_H

#include <string>

class CCurl {
    public:
        CCurl();
        ~CCurl();

        std::string m_source_html;
        std::string m_source_entete;
        uint16_t m_code_reponse;

        // On envooie avec post(URL, Conteneur) et on reçoit le retour
        void post(std::string, std::string, struct curl_slist * base_header = nullptr);
        void get(std::string, struct curl_slist * base_header = nullptr);
};
#endif
