# LibUAPVEnt  
  
Petite bibliothèque pour communiquer facilement avec l'Emploi du Temps de l'Université d'Avignon  
  
Créé à partir du code en Rust & Zig de [bilaliscarioth](https://gitlab.com/bilaliscarioth/uapv-api/ "Gitlab")  
  
2023, Fallingknife84 & bilaliscarioth
